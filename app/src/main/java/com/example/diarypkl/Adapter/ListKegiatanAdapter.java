package com.example.diarypkl.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.diarypkl.EditActivity;
import com.example.diarypkl.LoginActivity;
import com.example.diarypkl.Models.Aktivitas;
import com.example.diarypkl.R;
import com.example.diarypkl.Utilities.App;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

public class ListKegiatanAdapter  extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<Aktivitas> kegiatanList;
    private Calendar calendar;
    private SimpleDateFormat simpledateformat;
    private String Date;

    public ListKegiatanAdapter(List<Aktivitas> kegiatanList, Context context) {
        this.kegiatanList = kegiatanList;
        //Toast.makeText(context, ""+kegiatanList.size(), Toast.LENGTH_SHORT).show();
        calendar = Calendar.getInstance();
        simpledateformat = new SimpleDateFormat("yyyy-MM-dd");
        Date = simpledateformat.format(calendar.getTime());
    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ListKegiatanAdapterViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_kegiatan,parent,false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((ListKegiatanAdapterViewHolder)holder).bind(kegiatanList.get(position),position);
    }

    @Override
    public int getItemCount() {
        return kegiatanList.size();
    }

    private class ListKegiatanAdapterViewHolder extends RecyclerView.ViewHolder {

        TextView tanggal = (TextView)itemView.findViewById(R.id.tanggal);
        TextView penanggungjawab = (TextView)itemView.findViewById(R.id.penanggungjawab);
        TextView kegiatan = (TextView)itemView.findViewById(R.id.aktivitas);
        TextView lokasi = (TextView)itemView.findViewById(R.id.lokasi);
        Button Edit =(Button)itemView.findViewById(R.id.edit);
        public ListKegiatanAdapterViewHolder(View itemView) {
            super(itemView);
        }
        public void bind(final Aktivitas aktivitas, final int position) {
            kegiatan.setText(aktivitas.getPah_aktivitas());
            tanggal.setText(aktivitas.getPah_date());
            penanggungjawab.setText(aktivitas.getPah_penanggung_jawab());
            lokasi.setText(aktivitas.getPah_lokasi());
            if (Date.equals(aktivitas.getPah_date())){
                Edit.setVisibility(View.VISIBLE);
                Edit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(itemView.getContext(), EditActivity.class);
                        i.putExtra("id_aktivitas", aktivitas.getPah_id());
                        i.putExtra("isi_kegiatan", aktivitas.getPah_aktivitas());
                        i.putExtra("nama_pj", aktivitas.getPah_penanggung_jawab());
                        i.putExtra("tempat", aktivitas.getPah_lokasi());
                        itemView.getContext().startActivity(i);
                    }
                });
            }
            else{
                Edit.setVisibility(View.GONE);
            }
        }
    }
}
