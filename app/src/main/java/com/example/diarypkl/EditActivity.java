package com.example.diarypkl;

import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.diarypkl.Fragments.HomeFragment;
import com.example.diarypkl.Fragments.ListKegiatanFragment;
import com.example.diarypkl.Utilities.APIService;
import com.example.diarypkl.Utilities.APIUtils;

import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditActivity extends AppCompatActivity {

    private EditText penanggung1, lokasi1, kegiatan1;
    private Button edit;
    private ImageView back;
    private String  penanggung2, lokasi2, kegiatan2, pah_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);
        penanggung1= (EditText)findViewById(R.id.penanggung);
        lokasi1= (EditText)findViewById(R.id.lokasi);
        kegiatan1= (EditText)findViewById(R.id.kegiatan);
        edit=(Button)findViewById(R.id.edit);
        back=(ImageView)findViewById(R.id.back);
        pah_id = getIntent().getStringExtra("id_aktivitas");
        final String penanggung = getIntent().getStringExtra("nama_pj");
        final String kegiatan = getIntent().getStringExtra("isi_kegiatan");
        final String lokasi = getIntent().getStringExtra("tempat");
        penanggung1.setText(penanggung);
        lokasi1.setText(lokasi);
        kegiatan1.setText(kegiatan);
        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                penanggung2= capitalize(penanggung1.getText().toString());
                lokasi2=capitalize(lokasi1.getText().toString());
                kegiatan2=kegiatan1.getText().toString();
                edit_Pah(penanggung2,lokasi2,kegiatan2,pah_id);
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void edit_Pah(String penanggung_jawab, String lokasi, String aktivitas, String pah_id) {
        APIUtils.getAPIService().editPah(penanggung_jawab, lokasi, aktivitas,pah_id).enqueue(new Callback<HashMap<String, Object>>() {
            @Override
            public void onResponse(Call<HashMap<String, Object>> Response, Response<HashMap<String, Object>> response) {
                if((Double)response.body().get("status")== 200.0) {
                    Toast.makeText(EditActivity.this, "Edit Telah Berhasil Dilakukan", Toast.LENGTH_SHORT).show();
                    onBackPressed();
                }else {
                    Toast.makeText(EditActivity.this, "Gagal Upload", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<HashMap<String, Object>> call, Throwable t) {
                Toast.makeText(EditActivity.this, "Error: "+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private String capitalize(String capString){
        StringBuffer capBuffer = new StringBuffer();
        Matcher capMatcher = Pattern.compile("([a-z])([a-z]*)", Pattern.CASE_INSENSITIVE).matcher(capString);
        while (capMatcher.find()){
            capMatcher.appendReplacement(capBuffer, capMatcher.group(1).toUpperCase() + capMatcher.group(2).toLowerCase());
        }
        return capMatcher.appendTail(capBuffer).toString();
    }
}
