package com.example.diarypkl.Fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.diarypkl.LoginActivity;
import com.example.diarypkl.MainActivity;
import com.example.diarypkl.Models.Mahasiswa;
import com.example.diarypkl.R;
import com.example.diarypkl.Utilities.APIService;
import com.example.diarypkl.Utilities.APIUtils;
import com.example.diarypkl.Utilities.App;
import com.example.diarypkl.Utilities.PrefManager;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link HomeFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link HomeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HomeFragment extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private String mParam1;
    private String mParam2;
    private APIService mAPIService;
    private PrefManager pref;
    private EditText nim1,nama1,tempatpkl1,alamatpkl1,periodepkl1;
    private ImageView eye;
    private String nim,nama,tempatpkl,alamatpkl,periodepkl;

    private OnFragmentInteractionListener mListener;

    public HomeFragment() {
    }

    public static HomeFragment newInstance(String param1, String param2) {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

        mAPIService = APIUtils.getAPIService();
        pref = App.getInstance().getPref();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_home, container, false);
        nim1 = (EditText)view.findViewById(R.id.nim);
        nama1=(EditText)view.findViewById(R.id.nama);
        tempatpkl1=(EditText)view.findViewById(R.id.tempatpkl);
        periodepkl1=(EditText)view.findViewById(R.id.periodepkl);
        alamatpkl1=(EditText)view.findViewById(R.id.alamatpkl);
        getDataPeriodeByMahasiswaId();

        view.findViewById(R.id.btnlogout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                App.getInstance().getPref().logout();
                getActivity().finish();
                startActivity(new Intent(getActivity(), LoginActivity.class));
            }
        });
        return view;
    }

    private void getDataPeriodeByMahasiswaId() {
        mAPIService.getDataPeriodeByMahasiswaId(pref.getMahasiswa_id()).enqueue(new Callback<HashMap<String, Object>>() {
            @Override
            public void onResponse(Call<HashMap<String, Object>> Response, Response<HashMap<String, Object>> response) {
                if(response.body() != null) {
                    nama=response.body().get("mhs_nama").toString();
                    nim=response.body().get("mhs_nim").toString();
                    tempatpkl=response.body().get("tem_nama").toString();
                    alamatpkl=response.body().get("tem_alamat").toString();
                    periodepkl=response.body().get("per_nama").toString();
                    nim1.setText(nim);
                    nama1.setText(nama);
                    alamatpkl1.setText(alamatpkl);
                    tempatpkl1.setText(tempatpkl);
                    periodepkl1.setText(periodepkl);
                }else {
                    Toast.makeText(getActivity(), response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<HashMap<String, Object>> call, Throwable t) {
                Toast.makeText(getActivity(), "Error: "+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}
