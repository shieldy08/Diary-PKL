package com.example.diarypkl.Fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.diarypkl.R;
import com.example.diarypkl.Utilities.APIService;
import com.example.diarypkl.Utilities.APIUtils;
import com.example.diarypkl.Utilities.App;
import com.example.diarypkl.Utilities.PrefManager;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link KomentarFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link KomentarFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class KomentarFragment extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private EditText pesan1;
    private String pesan;
    private Button kirim;
    private APIService mAPIService;
    private PrefManager pref;
    private String mParam1;
    private String mParam2;
    private OnFragmentInteractionListener mListener;

    public KomentarFragment() {
    }

    public static KomentarFragment newInstance(String param1, String param2) {
        KomentarFragment fragment = new KomentarFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        mAPIService = APIUtils.getAPIService();
        pref = App.getInstance().getPref();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_komentar, container, false);
        pesan1 = (EditText)view.findViewById(R.id.komentar);
        kirim=(Button)view.findViewById(R.id.kirim);
        kirim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pesan=pesan1.getText().toString();
                kirimkomentar(pesan);
            }
        });
        return view;
    }

    private void kirimkomentar(String pesan) {
        mAPIService.kirimkomentar(pref.getMahasiswa_id(),pesan).enqueue(new Callback<HashMap<String, Object>>() {
            @Override
            public void onResponse(Call<HashMap<String, Object>> Response, Response<HashMap<String, Object>> response) {
                if((Double)response.body().get("status")== 200.0) {
                    Toast.makeText(getActivity(), "Komentar sudah terkirim", Toast.LENGTH_SHORT).show();
                    FragmentManager fm = getFragmentManager();
                    FragmentTransaction ft = fm.beginTransaction();
                    HomeFragment llf= new HomeFragment();
                    ft.replace(R.id.main_frame, llf);
                    ft.commit();
                }else {
                    Toast.makeText(getActivity(), "Error", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<HashMap<String, Object>> call, Throwable t) {
                Toast.makeText(getActivity(), "Error: "+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}
