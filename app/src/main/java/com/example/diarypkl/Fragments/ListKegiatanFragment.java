package com.example.diarypkl.Fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.diarypkl.Adapter.ListKegiatanAdapter;
import com.example.diarypkl.Models.Aktivitas;
import com.example.diarypkl.Models.DataAktivitas;
import com.example.diarypkl.R;
import com.example.diarypkl.Utilities.APIService;
import com.example.diarypkl.Utilities.APIUtils;
import com.example.diarypkl.Utilities.App;
import com.example.diarypkl.Utilities.PrefManager;
import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListKegiatanFragment extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private String mParam1;
    private String mParam2;
    private RecyclerView kegiatanView;
    private LinearLayoutManager linearLayoutManager;
    private List<Aktivitas> kegiatanList;
    private ListKegiatanAdapter listKegiatanAdapter;
    private ProgressDialog progressDialog;
    private APIService mAPIService;
    private PrefManager pref;
    private OnFragmentInteractionListener mListener;

    private void initial(View view){
        progressDialog = new ProgressDialog(getActivity());
    }
    public ListKegiatanFragment() {
    }

    public static ListKegiatanFragment newInstance(String param1, String param2) {
        ListKegiatanFragment fragment = new ListKegiatanFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        mAPIService = APIUtils.getAPIService();
        pref = App.getInstance().getPref();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.fragment_list_kegiatan, container, false);
        initial(view);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Loading...");
        kegiatanList = new ArrayList<>();
        kegiatanView = (RecyclerView)view.findViewById(R.id.recyclerview_pkl);
        listKegiatanAdapter = new ListKegiatanAdapter(kegiatanList,getActivity());
        linearLayoutManager = new LinearLayoutManager(getActivity());
        kegiatanView.setLayoutManager(linearLayoutManager);
        kegiatanView.setAdapter(listKegiatanAdapter);
        progressDialog.show();
        return view;
    }

    private void getListKegiatan() {
        kegiatanList.clear();
        mAPIService.get_aktivitas(pref.getMahasiswa_id()).enqueue(new Callback<DataAktivitas>() {
            @Override
            public void onResponse(Call<DataAktivitas> Response, Response<DataAktivitas> response) {
                for(Aktivitas aktivitas : response.body().getData()){
                    kegiatanList.add(aktivitas);
                }
                listKegiatanAdapter.notifyDataSetChanged();
                progressDialog.dismiss();
            }
            @Override
            public void onFailure(Call<DataAktivitas> call, Throwable t) {
                Toast.makeText(getActivity(), "Error: "+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onResume() { //supaya selalu manggil fungsi ini
        super.onResume();
        getListKegiatan();
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
