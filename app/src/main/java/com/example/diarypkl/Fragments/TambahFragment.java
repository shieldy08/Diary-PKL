package com.example.diarypkl.Fragments;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.method.TextKeyListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.example.diarypkl.R;
import com.example.diarypkl.Utilities.APIService;
import com.example.diarypkl.Utilities.APIUtils;
import com.example.diarypkl.Utilities.App;
import com.example.diarypkl.Utilities.PrefManager;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link TambahFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link TambahFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TambahFragment extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private String mParam1;
    private String mParam2;
    private EditText tanggal, penanggung1, lokasi1, kegiatan1;
    private Button kirim;
    private Calendar calendar;
    private SimpleDateFormat simpledateformat;
    private String Date, penanggung,lokasi,kegiatan;
    private APIService mAPIService;
    private PrefManager pref;
    private OnFragmentInteractionListener mListener;

    public TambahFragment() {
    }

    public static TambahFragment newInstance(String param1, String param2) {
        TambahFragment fragment = new TambahFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        mAPIService = APIUtils.getAPIService();
        pref = App.getInstance().getPref();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tambah, container, false);
        calendar = Calendar.getInstance();
        simpledateformat = new SimpleDateFormat("yyyy-MM-dd");
        Date = simpledateformat.format(calendar.getTime());
        tanggal= (EditText) view.findViewById(R.id.calender);
        tanggal.setText(Date);
        penanggung1= (EditText) view.findViewById(R.id.penanggung);
        lokasi1= (EditText) view.findViewById(R.id.lokasi);
        kegiatan1= (EditText) view.findViewById(R.id.kegiatan);
        kirim=(Button)view.findViewById(R.id.kirim);
        kirim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                penanggung= capitalize(penanggung1.getText().toString());
                lokasi=capitalize(lokasi1.getText().toString());
                kegiatan=kegiatan1.getText().toString();
                kirimdataharian(Date,penanggung,lokasi,kegiatan);
            }
        });
        return view;
    }

    private void kirimdataharian(String date, String penanggung_jawab, String lokasi, String aktivitas) {
        mAPIService.kirimdataharian(date, penanggung_jawab, lokasi, aktivitas, pref.getMahasiswa_id()).enqueue(new Callback<HashMap<String, Object>>() {
            @Override
            public void onResponse(Call<HashMap<String, Object>> Response, Response<HashMap<String, Object>> response) {
                if((Double)response.body().get("status")== 200.0) {
                    Toast.makeText(getActivity(), "Data Sudah Berhasil terinput", Toast.LENGTH_SHORT).show();
                    FragmentManager fm = getFragmentManager();
                    FragmentTransaction ft = fm.beginTransaction();
                    HomeFragment llf= new HomeFragment();
                    ft.replace(R.id.main_frame, llf);
                    ft.commit();
                }else {
                    Toast.makeText(getActivity(), "Error", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<HashMap<String, Object>> call, Throwable t) {
                Toast.makeText(getActivity(), "Error: "+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private String capitalize(String capString){
        StringBuffer capBuffer = new StringBuffer();
        Matcher capMatcher = Pattern.compile("([a-z])([a-z]*)", Pattern.CASE_INSENSITIVE).matcher(capString);
        while (capMatcher.find()){
            capMatcher.appendReplacement(capBuffer, capMatcher.group(1).toUpperCase() + capMatcher.group(2).toLowerCase());
        }
        return capMatcher.appendTail(capBuffer).toString();
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}
