package com.example.diarypkl;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.diarypkl.Models.Mahasiswa;
import com.example.diarypkl.Utilities.APIService;
import com.example.diarypkl.Utilities.APIUtils;
import com.example.diarypkl.Utilities.App;
import com.example.diarypkl.Utilities.PrefManager;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {
    private Button login;
    private EditText email1,password1;
    private ImageView eye;
    private String email,password;
    private boolean hide=true;
    private APIService mAPIService;
    private ProgressDialog progressDialog;
    private PrefManager pref;

    private void initial(){
        pref = App.getInstance().getPref();
        mAPIService = APIUtils.getAPIService();
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        login=(Button)findViewById(R.id.buttonlogin);
        email1 = (EditText) findViewById(R.id.email);
        password1=(EditText)findViewById(R.id.password);
        eye = (ImageView)findViewById(R.id.eye);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initial();
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                email=email1.getText().toString();
                password=password1.getText().toString();
                login(email, password);
            }
        });
        eye.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(hide){
                    password1.setInputType(InputType.TYPE_CLASS_TEXT);
                    eye.setImageResource(R.drawable.eye1);
                    hide=false;
                }
                else {
                    password1.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    eye.setImageResource(R.drawable.eye);
                    hide=true;
                }
            }
        });

    }

    private void login(String email, String password) {
        progressDialog.show();
        mAPIService.login(email, password).enqueue(new Callback<Mahasiswa>() {
            @Override
            public void onResponse(Call<Mahasiswa> Response, Response<Mahasiswa> response) {
                if(response.body() != null) {
                    if (response.body().getMahasiswa_id() != null) {
                        pref.setNim(response.body().getNim());
                        pref.setEmail(response.body().getEmail());
                        pref.setNama(response.body().getNama());
                        pref.setMahasiswa_id(response.body().getMahasiswa_id());
                        pref.setIsLogin(true);
                        Intent i = new Intent(LoginActivity.this, MainActivity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(i);
                    }else {
                        Toast.makeText(LoginActivity.this, "Username atau password salah", Toast.LENGTH_SHORT).show();
                    }
                    progressDialog.dismiss();
                }else {
                    Toast.makeText(LoginActivity.this, "username atau password salah", Toast.LENGTH_SHORT).show();
                }
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<Mahasiswa> call, Throwable t) {
                Toast.makeText(LoginActivity.this, "Error: "+t.getMessage(), Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }
}
