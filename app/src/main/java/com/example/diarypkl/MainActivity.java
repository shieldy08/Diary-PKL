package com.example.diarypkl;

import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.annotation.NonNull;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.example.diarypkl.Fragments.HomeFragment;
import com.example.diarypkl.Fragments.KomentarFragment;
import com.example.diarypkl.Fragments.ListKegiatanFragment;
import com.example.diarypkl.Fragments.TambahFragment;

public class MainActivity extends AppCompatActivity {
    private TextView mTextMessage;
    private Boolean doubleBackToExitPressedOnce = false;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.home_kegiatan:
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.main_frame, new HomeFragment())
                            .commit();
                    return true;
                case R.id.list_kegiatan:
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.main_frame, new ListKegiatanFragment())
                            .commit();
                    return true;
                case R.id.tambah_kegiatan:
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.main_frame, new TambahFragment())
                            .commit();
                    return true;
                case R.id.komentar:
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.main_frame, new KomentarFragment())
                            .commit();
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        BottomNavigationView navView = findViewById(R.id.nav_view);
        mTextMessage = findViewById(R.id.message);
        navView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.main_frame, new HomeFragment())
                .commit();

    }

    public void onBackPressed() {
        FragmentManager fm  = getSupportFragmentManager();
        if(fm.getBackStackEntryCount() > 0) {
            fm.popBackStack();
        }else {
            if (doubleBackToExitPressedOnce) {
                ActivityCompat.finishAffinity(MainActivity.this);
                return;
            }

            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, "Please click back again to exit", Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce=false;
                }
            }, 2000);
        }
    }

}
