package com.example.diarypkl.Models;

public class Aktivitas {
    private String pah_id, pes_id,pah_date,pah_penanggung_jawab;
    private String pah_lokasi,pah_aktivitas,pah_konfirmasi;

    public String getPah_id() {
        return pah_id;
    }

    public void setPah_id(String pah_id) {
        this.pah_id = pah_id;
    }

    public String getPes_id() {
        return pes_id;
    }

    public void setPes_id(String pes_id) {
        this.pes_id = pes_id;
    }

    public String getPah_date() {
        return pah_date;
    }

    public void setPah_date(String pah_date) {
        this.pah_date = pah_date;
    }

    public String getPah_penanggung_jawab() {
        return pah_penanggung_jawab;
    }

    public void setPah_penanggung_jawab(String pah_penanggung_jawab) {
        this.pah_penanggung_jawab = pah_penanggung_jawab;
    }

    public String getPah_lokasi() {
        return pah_lokasi;
    }

    public void setPah_lokasi(String pah_lokasi) {
        this.pah_lokasi = pah_lokasi;
    }

    public String getPah_aktivitas() {
        return pah_aktivitas;
    }

    public void setPah_aktivitas(String pah_aktivitas) {
        this.pah_aktivitas = pah_aktivitas;
    }

    public String getPah_konfirmasi() {
        return pah_konfirmasi;
    }

    public void setPah_konfirmasi(String pah_konfirmasi) {
        this.pah_konfirmasi = pah_konfirmasi;
    }
}
