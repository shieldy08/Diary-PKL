package com.example.diarypkl.Models;

import java.util.List;

public class DataAktivitas {
    private List<Aktivitas> data;

    public List<Aktivitas> getData() {
        return data;
    }

    public void setData(List<Aktivitas> data) {
        this.data = data;
    }
}
