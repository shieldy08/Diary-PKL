package com.example.diarypkl.Models;

public class Mahasiswa {

    private String email, jenis_akun, nama,nim, mahasiswa_id;
    private boolean is_admin;
    private int status;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getJenis_akun() {
        return jenis_akun;
    }

    public void setJenis_akun(String jenis_akun) {
        this.jenis_akun = jenis_akun;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getNim() {
        return nim;
    }

    public void setNim(String nim) {
        this.nim = nim;
    }

    public String getMahasiswa_id() {
        return mahasiswa_id;
    }

    public void setMahasiswa_id(String mahasiswa_id) {
        this.mahasiswa_id = mahasiswa_id;
    }

    public boolean isIs_admin() {
        return is_admin;
    }

    public void setIs_admin(boolean is_admin) {
        this.is_admin = is_admin;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
