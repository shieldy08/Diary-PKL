package com.example.diarypkl;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Space;

import com.example.diarypkl.Utilities.App;
import com.example.diarypkl.Utilities.PrefManager;

public class SplashActivity extends AppCompatActivity {
    private PrefManager pref;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);

        pref = App.getInstance().getPref();

        (new Handler()).postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent i;
                if(pref.getIsLogin()){
                    i = new Intent(SplashActivity.this, MainActivity.class);
                }else {
                    i = new Intent(SplashActivity.this, LoginActivity.class);
                }
                finish();
                startActivity(i);
            }
        }, 1500);
    }
}
