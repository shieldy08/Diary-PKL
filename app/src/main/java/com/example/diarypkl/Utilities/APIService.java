package com.example.diarypkl.Utilities;

import com.example.diarypkl.Models.DataAktivitas;
import com.example.diarypkl.Models.Mahasiswa;

import java.util.HashMap;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface APIService {

    @Headers({
            "Accept: application/json",
            "authority: "+ Constant.token
    })

    @POST("API/auth")
    @FormUrlEncoded
    Call<Mahasiswa> login (@Field("user") String user,
                          @Field("password") String password);

    @Headers({
            "Accept: application/json",
            "authority: "+ Constant.token
    })
    @POST("API/get_data_periode_by_mahasiswa_id")
    @FormUrlEncoded
    Call<HashMap<String, Object>> getDataPeriodeByMahasiswaId (
            @Field("mahasiswa_id") String mahasiswa_id
    );

    @Headers({
            "Accept: application/json",
            "authority: "+ Constant.token
    })
    @POST("API/insert_pah")
    @FormUrlEncoded
    Call<HashMap<String, Object>> kirimdataharian(
            @Field("date") String date,
            @Field("penanggung_jawab") String penanggung_jawab,
            @Field("lokasi") String lokasi,
            @Field("aktivitas") String aktivitas,
            @Field("mahasiswa_id") String mahasiswa_id
    );

    @Headers({
            "Accept: application/json",
            "authority: "+ Constant.token
    })
    @POST("API/insert_komentar")
    @FormUrlEncoded
    Call<HashMap<String, Object>> kirimkomentar(
            @Field("mahasiswa_id") String mahasiswa_id,
            @Field("pesan") String pesan
    );

    @Headers({
            "Accept: application/json",
            "authority: "+ Constant.token
    })
    @POST("API/get_data_pah")
    @FormUrlEncoded
    Call<DataAktivitas> get_aktivitas(
            @Field("mahasiswa_id") String mahasiswa_id
    );

    @Headers({
            "Accept: application/json",
            "authority: "+ Constant.token
    })
    @POST("API/update_pah")
    @FormUrlEncoded
    Call<HashMap<String, Object>> editPah(
            @Field("penanggung_jawab") String penanggung_jawab,
            @Field("lokasi") String lokasi,
            @Field("aktivitas") String aktivitas,
            @Field("pah_id") String pah_id
    );
}
