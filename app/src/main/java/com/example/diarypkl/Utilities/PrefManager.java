package com.example.diarypkl.Utilities;

import android.content.Context;
import android.content.SharedPreferences;

public class PrefManager {

    private static final String PREF_NAME = "DiarySharedPreferences";
    private final SharedPreferences pref;
    private static final int PRIVATE_MODE = 0;
    private static final String KEY_EMAIL = "k1";
    private static final String KEY_JENIS_AKUN = "k2";
    private static final String KEY_NAMA = "k3";
    private static final String KEY_NIM = "k4";
    private static final String KEY_MAHASISWA_ID = "k5";
    private static final String KEY_IS_ADMIN = "k6";
    private static final String KEY_IS_LOGIN = "k8";

    public PrefManager(Context context) {
        pref = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
    }
    public void logout(){
        SharedPreferences.Editor editor = pref.edit();
        setMahasiswa_id("");
        setNim("");
        setNama("");
        setEmail("");
        setJenis_akun("");
        setIs_admin(false);
        setIsLogin(false);
        editor.apply();
    }

    public void setEmail(String email){
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(KEY_EMAIL, email);
        editor.apply();;
    }
    public String getEmail(){
        return pref.getString(KEY_EMAIL, "No Email");
    }

    public String getJenisAkun(){
        return pref.getString(KEY_JENIS_AKUN, "None");
    }
    public void setJenis_akun(String jenis_akun){
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(KEY_JENIS_AKUN, jenis_akun);
        editor.apply();;
    }

    public void setNama(String nama) {
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(KEY_NAMA, nama);
        editor.apply();
        ;
    }
    public String getNama(){ return pref.getString(KEY_NAMA, "No Name");
    }

    public void setNim(String nim){
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(KEY_NIM, nim);
        editor.apply();;
    }
    public String getNim(){
        return pref.getString(KEY_EMAIL, "0");
    }

    public void setMahasiswa_id(String mahasiswa_id){
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(KEY_MAHASISWA_ID, mahasiswa_id);
        editor.apply();;
    }
    public String getMahasiswa_id(){
        return pref.getString(KEY_MAHASISWA_ID, "0");
    }

    public void setIs_admin(boolean is_admin){
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean(KEY_IS_ADMIN, is_admin);
        editor.apply();;
    }
    public Boolean getIs_admin(){
        return pref.getBoolean(KEY_IS_ADMIN, false);
    }

    public void setIsLogin(Boolean IsLogin){
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean(KEY_IS_LOGIN, IsLogin);
        editor.apply();;
    }
    public Boolean getIsLogin(){
        return pref.getBoolean(KEY_IS_LOGIN, false);
    }
}
